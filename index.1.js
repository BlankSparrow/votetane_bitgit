var express = require('express');
var app = express();
var pgp = require('pg-promise')();
var Fingerprint = require('express-fingerprint')
var bodyParser = require('body-parser')

var db = pgp(process.env.DATABASE_URL);
// --------------------------------------------------------------------------------------------------------
// Setting default values
// --------------------------------------------------------------------------------------------------------
pg.defaults.ssl = true;

app.use(bodyParser.urlencoded({
    extended: true
}))


app.use(Fingerprint({
    paramters:[
        // Defaults 
        Fingerprint.useragent,
        Fingerprint.acceptHeaders,
        Fingerprint.geoip,
         // Additional parameters 
        function(next) {
            // ...do something... 
            next(null,{
            'param1':'value1'
            })
        },
        function(next) {
            // ...do something... 
            next(null,{
            'param2':'value2'
            })
        },
    ]
}))

app.set('port', (process.env.PORT || 5000));

app.use(express.static(__dirname + '/public'));

// views is directory for all template files
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

///--------------------------------------------------------------------------------------------------------
/// Database setup
///--------------------------------------------------------------------------------------------------------
function sql(file) {
    // consider using here: path.join(__dirname, file)
    return new pgp.QueryFile(file, {minify: true});
}

// Create QueryFile globally, once per file:
var sql_wyr_finquestion = sql('./sql/wyr_findquestion.sql');

// --------------------------------------------------------------------------------------------------------
// Home Page (votetane.com/)
// --------------------------------------------------------------------------------------------------------
app.get('/', function(request, response) {
  response.render('pages/index');
});

// --------------------------------------------------------------------------------------------------------
// Would You Rather Page (votetane.com/game)
// --------------------------------------------------------------------------------------------------------
app.get('/game', function(request, response) {
  response.redirect('/game/' + Math.ceil(Math.random() * 38));
});

/*
app.post('/game', function(request, response) {
  pg.connect(process.env.DATABASE_URL, function(err, client) {
    if (err){
    }else{
      
      console.log('Connected to postgres! Getting schemas...');
      client.query("INSERT INTO wyr_data(fingerprint, question_number, question_answer) VALUES ('" + request.fingerprint.hash +"', "+ parseInt(request.body.question_number) +", "+ parseInt(request.body.question_answer) + ")");
      
      pg.end(function (err) {
        if (err) throw err;
      });
    }
    pg.end(function (err) {
    });
  });
   //client.end();
   //response.end("yes");
});*/


app.get('/game/:question_number', function(request, response) {
  var question_1 = 'question_1';
  var question_2 = 'question_2';
  
  db.one(sql_wyr_finquestion, {question_number: request.params.question_number})
    .then(data=> {
        question_1 = data.question_1;
        question_2 = data.question_2;
    })
    .catch(error=> {
        if (error instanceof pgp.errors.QueryFileError) {
            // => the error is related to our QueryFile
        }
    });
  response.render('pages/game',  {question_1: question_1, question_2: question_2, question_number: request.params.question_number});
     
  /*
  pg.connect(process.env.DATABASE_URL, function(err, client) {
  if (err) {
    console.log('Failed to connect to postgres!loading default...');
    response.render('pages/game',  {question_1: question_1, question_2: question_2, question_number: request.params.question_number});
     client.end();
  }else{
  console.log('Connected to postgres! Getting schemas...');

  client.query('SELECT question_1, question_2 FROM wyr_list where id='+request.params.question_number, ['foo'], function(err, res) {
        console.log(res.rows[0].name);
        client.end();
    });
  var query_questions = client.query('SELECT question_1, question_2 FROM wyr_list where id='+request.params.question_number);
  query_questions.on('row', function(row){
    //var result = JSON.parse(row);
    question_1 = row.question_1;
    question_2 = row.question_2;
    client.end();
    response.render('pages/game',  {question_1: question_1, question_2: question_2, question_number: request.params.question_number});
   
  });

   pg.end(function (err) {
        if (err) throw err;
    });
  }
  });
  client
    .query('SELECT row_to_json(wyr_list) FROM wyr_list where id='+request.params.question_number)
    .on('row', function(row) {
      tagline += JSON.stringify(row);
      console.log('addr: '+request.ip);
    });

    response.render('pages/game',  {question_1: question_1, question_2: question_2, question_number: request.params.question_number});
    pg.end(function (err) {
        if (err) throw err;
    });
  });*/

  
  
  
});


// --------------------------------------------------------------------------------------------------------
// Console Page (votetane.com/console)
// --------------------------------------------------------------------------------------------------------

app.get('/console', function(request, response) {
    response.render('pages/about', {tagline: request.fingerprint.hash});
});

// --------------------------------------------------------------------------------------------------------
// About Page (votetane.com/about)
// --------------------------------------------------------------------------------------------------------
app.get('/about', function(request, response) {
  var tagline = 'hello';
  response.render('pages/about', { tagline: tagline });
  });

// --------------------------------------------------------------------------------------------------------
// Connect Page (votetane.com/connect)
// --------------------------------------------------------------------------------------------------------
app.get('/connect', function(request, response) {
  var tagline;
  pg.defaults.ssl = true;/*
  pg.connect(process.env.DATABASE_URL, function(err, client) {
  if (err) throw err;
  console.log('Connected to postgres! Getting schemas...');

  client
    .query('SELECT table_schema,table_name FROM information_schema.tables;')
    .on('row', function(row) {
      
      tagline += JSON.stringify(row);
      console.log('addr: '+request.ip);

  

      response.render('pages/about', { tagline: tagline });
      pg.end(function (err) {
      if (err) throw err;
    });
    });
  });*/
});

// --------------------------------------------------------------------------------------------------------
// Starting server
// --------------------------------------------------------------------------------------------------------
app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});


