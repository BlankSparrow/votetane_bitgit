var express = require('express');
var app = express();
var pgp = require('pg-promise')();
var Fingerprint = require('express-fingerprint')
var bodyParser = require('body-parser')

var db = pgp(process.env.DATABASE_URL);
// --------------------------------------------------------------------------------------------------------
// Setting default values
// --------------------------------------------------------------------------------------------------------
//app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({
    extended: true
}))


app.use(Fingerprint({
    paramters:[
        // Defaults 
        Fingerprint.useragent,
        Fingerprint.acceptHeaders,
        Fingerprint.geoip,
         // Additional parameters 
        function(next) {
            // ...do something... 
            next(null,{
            'param1':'value1'
            })
        },
        function(next) {
            // ...do something... 
            next(null,{
            'param2':'value2'
            })
        },
    ]
}))

app.set('port', (process.env.PORT || 5000));

app.use(express.static(__dirname + '/public'));

// views is directory for all template files
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

///--------------------------------------------------------------------------------------------------------
/// Database setup
///--------------------------------------------------------------------------------------------------------
function sql(file) {
    // consider using here: path.join(__dirname, file)
    return new pgp.QueryFile(file, {minify: true});
}

// Create QueryFile globally, once per file:
var sql_wyr_finquestion = sql('./sql/wyr_findquestion.sql');
var sql_wyr_insertanswer = sql('./sql/wyr_insertanswer.sql');
var sql_wyr_getstats = sql('./sql/wyr_getstats.sql');
var sql_cs_insert_data = sql('./sql/cs_insertdata.sql');

// --------------------------------------------------------------------------------------------------------
// Home Page (votetane.com/)
// --------------------------------------------------------------------------------------------------------
app.get('/', function(request, response) {
     var people = [
      {name:'Alexandra Davids', location:'Linwood',                 image:'/img/alexandra_davids.jpg',  link:'https://www.facebook.com/Alexandra.Davids.Council/'},
      {name:'Mike Davidson',    location:'Papanui',                 image:'/img/mike_davidson.jpg',     link:'https://www.facebook.com/Vote-Mike-Davidson-Councillor-for-Papanui-1675863245975747/'},
      {name:'Deon Swiggs',      location:'Central',                 image:'/img/deon_swiggs.jpg',       link:'https://www.facebook.com/DeonSwiggsNZ/'},
      {name:'Kevin Clarke',     location:'Halswell',                image:'/img/kevin_clarke.jpg',      link:'https://www.facebook.com/kevinclarke4council/'},
      {name:'Claire Exton',     location:'Hornby Community Board',  image:'/img/claire_exton.jpg',      link:'https://www.facebook.com/groups/1580495372245840/'},
      {name:'Sara Templeton',   location:'Heathcoate',              image:'/img/sara_templeton.jpg',    link:'https://www.facebook.com/saratempletonchch/'}
  ];
  response.render('pages/index', { people: people });
});

// --------------------------------------------------------------------------------------------------------
// About Page (votetane.com/about)
// --------------------------------------------------------------------------------------------------------
app.get('/about', function(request, response) {
  var tagline = 'hello';
  response.render('pages/about', { tagline: tagline });
  });


// --------------------------------------------------------------------------------------------------------
// Test Page (votetane.com/about)
// --------------------------------------------------------------------------------------------------------
app.get('/test', function(request, response) {
 
  response.render('pages/console', { people: people });
  });

// --------------------------------------------------------------------------------------------------------
// My goals Page (votetane.com/about)
// --------------------------------------------------------------------------------------------------------
app.get('/goals', function(request, response) {
  var tagline = 'hello';
  response.render('pages/goals', { tagline: tagline });
  });
// --------------------------------------------------------------------------------------------------------
// Would You Rather Page (votetane.com/game)
// --------------------------------------------------------------------------------------------------------
app.get('/game', function(request, response) {
  response.redirect('/game/' + Math.ceil(Math.random() * 38));
});


app.get('/game/:question_number', function(request, response) {
  db.one(sql_wyr_finquestion, {question_number: request.params.question_number})
    .then(data=> {
        response.render('pages/game',  {question_1: data.question_1, question_2: data.question_2, question_number: request.params.question_number});
    })
    .catch(error=> {
        if (error instanceof pgp.errors.QueryFileError) {
            // => the error is related to our QueryFile
        }
    });
});

app.post('/game', function(req, response) {
  db.none(sql_wyr_insertanswer , {fingerprint: req.fingerprint.hash, question_number: req.body.question_number, question_result: req.body.question_result})
    .then( function(){
    })
    .catch(function (error) {
        // error;
    });
    db.any(sql_wyr_getstats,{question_number: req.body.question_number})
  .then( function(data){
      //console.log(req.body.question_number)
      console.log(data)
      response.send(data)
    })
    .catch(function (error) {
        // error;
    });
});


// --------------------------------------------------------------------------------------------------------
// Cantrax page(votetane.com/cantrax)
// --------------------------------------------------------------------------------------------------------

app.get('/cantrax', function(request, response) {   
  response.render('pages/cantrax');
});

app.get('/cantrax_survey', function(request, response) {   
  response.render('pages/cantrax_survey');
});

app.post('/cantrax', function(req, response) {
    console.log(req.body)
    response.send(req.body)
    db.none(sql_cs_insert_data, {fingerprint: req.fingerprint.hash, location_1: req.body.location_1, location_2: req.body.location_2, cost_1: req.body.cost_1, usage_1: req.body.usage_1})
    .then( function(){
        
    })
    .catch(function (error) {
        // error;
    });
    console.log(req.body);
    response.send(req.body);
});

// --------------------------------------------------------------------------------------------------------
// Starting server
// --------------------------------------------------------------------------------------------------------
app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});


